package apiwork

import (
	"log"
	"io/ioutil"
	"fmt"
	"net/http"
	"encoding/json"
	"domain"
)

func GetWeather(apiKey string, longitude float64, latitude float64) string{
	

	url := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s", latitude, longitude, apiKey)

	response, err:= http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	var weatherResponse WeatherResponse
	err = json.Unmarshal(body, &weatherResponse)
	if err != nil {
		log.Fatal(err)
	}

	//log.Println("urlline:", fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&appid=%s", latitude, longitude, apiKey))
	//log.Println("Status code:", response.StatusCode)

	if len(weatherResponse.Weather) > 0 {
		mainweather := weatherResponse.Weather[0]
		return mainweather.Main
	} else {
		return "no weather"
	}
}